let gates = [];

let selected_Gate;
let paperCurrentTranslateX = 0;
let paperCurrentTranslateY = 0;
let paperCurrentScale = 1;
let placeObj = false;
class Gate {
  constructor(type, event) {
    this.id = Math.floor(Math.random() * 100);
    this.element = new joint.shapes.standard.Rectangle();
    // this.element = new joint.shapes.basic.Rect({
    //   size: { width: 100, height: 60 },
    //   image: { xLinkHref: 'src/Nand.png' }
    // });
    this.element.position(
      event.clientX - paperCurrentTranslateX,
      event.clientY
    );
    if (type == "Output" || type == "Input") {
      this.element.resize(40, 40);
    } else {
      this.element.resize(100, 40);
    }
    var inputPort = {
      // label: {
      //     position: {
      //         name: 'left'
      //     },
      //     markup: [{
      //         tagName: 'text',
      //         selector: 'label'
      //     }]
      // },
      attrs: {
        portBody: {
          magnet: true,
          width: 16,
          height: 16,
          x: -8,
          y: -8,
          fill: "#03071E",
        },
        label: {
          text: "port",
        },
      },
      markup: [
        {
          tagName: "rect",
          selector: "portBody",
        },
      ],
    };
    var outputPort = {
      position: { name: "right" },
      // label: {
      //     position: {
      //         name: 'right'
      //     },
      //     markup: [{
      //         tagName: 'text',
      //         selector: 'label'
      //     }]
      // },
      attrs: {
        portBody: {
          magnet: true,
          width: 16,
          height: 16,
          x: 90,
          y: -20,
          fill: "#03071E",
        },
        label: {
          text: "port",
        },
      },
      markup: [
        {
          tagName: "rect",
          selector: "portBody",
        },
      ],
    };
    this.element.attr({
      body: {
        fill: "blue",
      },
      label: {
        text: type,
        fill: "white",
      },
      ports: {
        items: [inputPort, outputPort], // add a port in constructor
      },
    });
    if (type == "Not") {
      this.element.addPort(inputPort); // add a port using Port API
      this.element.addPort(outputPort); // add a port using Port API
    } else if (type == "Nand") {
      this.element.addPort(inputPort); // add a port using Port API
      this.element.addPort(inputPort); // add a port using Port API
      this.element.addPort(outputPort); // add a port using Port API
    } else if (type == "Input") {
      this.element.addPort(outputPort); // add a port using Port API
    } else {
      this.element.addPort(inputPort); // add a port using Port API
    }
    this.element.addTo(graph);
    placeObj = false;
  }
}
$(document).ready(function () {
  $("#NandGate").click(function () {
    selected_Gate = "Nand";
    placeObj = true;
  });
  $("#NotGate").click(function () {
    selected_Gate = "Not";
    placeObj = true;
  });
  $("#Output").click(function () {
    selected_Gate = "Output";
    placeObj = true;
  });
  $("#Input").click(function () {
    selected_Gate = "Input";
    placeObj = true;
  });
  $("#miniMapHolder")
    .bind("mouseenter", function () {
      $("#paper-multiple-papers-small").show();
      $("#miniMapButton").hide();
    })
    .bind("mouseleave", function () {
      $("#paper-multiple-papers-small").hide();
      $("#miniMapButton").show();
    });
  $("#panLeft").click(function () {
    paperCurrentTranslateX -= 10;
    paper.translate(paperCurrentTranslateX, paperCurrentTranslateY);
    paperSmall.translate(paperCurrentTranslateX, paperCurrentTranslateY);
  });
  $("#panRight").click(function () {
    paperCurrentTranslateX += 10;
    paper.translate(paperCurrentTranslateX, paperCurrentTranslateY);
    paperSmall.translate(paperCurrentTranslateX, paperCurrentTranslateY);
  });
  $("#panUp").click(function () {
    paperCurrentTranslateY -= 10;
    paper.translate(paperCurrentTranslateX, paperCurrentTranslateY);
    paperSmall.translate(paperCurrentTranslateX, paperCurrentTranslateY);
  });
  $("#panDown").click(function () {
    paperCurrentTranslateY += 10;
    paper.translate(paperCurrentTranslateX, paperCurrentTranslateY);
    paperSmall.translate(paperCurrentTranslateX, paperCurrentTranslateY);
  });
  $("#ZoomIn").click(function () {
    paperCurrentScale += 0.1;
    paper.scale(paperCurrentScale);
  });
  $("#ZoomOut").click(function () {
    paperCurrentScale -= 0.1;
    paper.scale(paperCurrentScale);
  });
  $("#resetView").click(function () {
    paperCurrentTranslateX = 0;
    paperCurrentTranslateY = 0;
    paperCurrentScale = 1;
    paper.scale(paperCurrentScale);
    paper.translate(paperCurrentTranslateX, paperCurrentTranslateY);
    paperSmall.translate(paperCurrentTranslateX, paperCurrentTranslateY);
  });
  let screenLog = document.querySelector("#screen-log");
  document.addEventListener("mousemove", logKey);
  function logKey(e) {
    screenLog.innerText = `Screen X/Y: ${e.screenX}, ${e.screenY}, Client X/Y: ${e.clientX}, ${e.clientY}`;
  }
  function xlogKeyReturney(e) {
    // return e.clientX + paperCurrentTranslateX + $('sidebar').width();
    return e.screenX;
  }
  function ylogKeyReturney(e) {
    // return e.clientY + paperCurrentTranslateY;
    return e.screenY;
  }
  $("#myholder").click(function (event) {
    if (placeObj == true) {
      gates.push(new Gate(selected_Gate, event));
    }
  });

  //Experimental !!!

  $("#gatesDropdown")
    .bind("mouseenter", function () {
      $(".dropdown-content").show();
      // $('#miniMapButton').hide();
    })
    .bind("mouseleave", function () {
      $(".dropdown-content").hide();
      // $('#miniMapButton').show();
    });
});
