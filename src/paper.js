class paper {
  constructor() {
    var namespace = joint.shapes;

    var graph = new joint.dia.Graph({}, { cellNamespace: namespace });

    var paper = new joint.dia.Paper({
      el: document.getElementById("myholder"),
      model: graph,
      width: $(this).parent().width(),
      height: $(window).height(),
      gridSize: 35,
      drawGrid: true,
      drawGrid: { name: "dot", args: { color: "whitesmoke" } },
      defaultLink: () => new joint.shapes.standard.Link(),
      linkPinning: false,
      background: {
        color: "#1e293b",
      },
      cellViewNamespace: namespace,
    });

    // Translate the paper at the first stage
    // paper.translate($('.sidebar').width(),0)

    // minimap paper view
    var paperSmall = new joint.dia.Paper({
      el: document.getElementById("paper-multiple-papers-small"),
      model: graph,
      width: $(window).width() * 0.25,
      height: $(window).height() * 0.25,
      gridSize: 1,
      interactive: false,
      background: {
        color: "#0f172a",
      },
      cellViewNamespace: namespace,
    });
    paperSmall.scale(0.25);
  }
}
